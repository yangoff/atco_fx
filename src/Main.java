import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/initial.fxml"));

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 1000,600));
        primaryStage.show();

    }


    public static void main(String[] args) {

        launch(args);


//        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("atco");
//        EntityManager manager = entityManagerFactory.createEntityManager();
//        manager.close();
//        entityManagerFactory.close();
    }
}
