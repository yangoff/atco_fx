package model;

public enum Praga {
    Barata("1"),
    Rato("2"),
    Formiga("3"),
    Carrapato("4"),
    Aranha("5"),
    Caruncho("6"),
    Escorpiao("7"),
    Morcego("8"),
    Traca("9"),
    Mosca("10"),
    Outros("11");

    private String praga;

    Praga(String s) {
        this.praga = s;
    }

    public String getPraga() {
        return praga;
    }
}
