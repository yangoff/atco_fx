package model;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "os")
public class Os {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String name;


    private int registro;


    private Date data;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "os_produto",
            joinColumns = { @JoinColumn(name = "os_id") },
            inverseJoinColumns = { @JoinColumn(name = "produto_id") }
    )
    Set<Produto> produtos = new HashSet<>();

    @ElementCollection
    private List<String> pragas = new ArrayList<>();

    @OneToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="tcp_id", unique= true, nullable=true, insertable=true, updatable=true)
    private Tcp tcp;

    private Enum Vendedor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Set<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }

    public List<String> getPragas() {
        return pragas;
    }

    public void setPragas(List<String> pragas) {
        this.pragas = pragas;
    }

    public Tcp getTcp() {
        return tcp;
    }

    public void setTcp(Tcp tcp) {
        this.tcp = tcp;
    }

    public Enum getVendedor() {
        return Vendedor;
    }

    public void setVendedor(Enum vendedor) {
        Vendedor = vendedor;
    }
}
