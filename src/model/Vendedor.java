package model;

public enum Vendedor {
    Juliana("Juliana"),
    Tatiana("Tatiana");

    private String vendedor;

    Vendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getVendedor() {
        return vendedor;
    }
}
