package model;

import javax.persistence.*;

@Entity
@Table(name = "Tcp")
public class Tcp {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String name;


    private int registro;


    private int nivel;

    public Tcp(String name, int registro, int nivel) {
        this.name = name;
        this.registro = registro;
        this.nivel = nivel;
    }

    public Tcp(Long id, String name, int registro, int nivel) {
        this.id = id;
        this.name = name;
        this.registro = registro;
        this.nivel = nivel;
    }

    public Tcp(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    @Override
    public String toString() {
        return "Tcp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", registro=" + registro +
                ", nivel=" + nivel +
                '}';
    }
}
