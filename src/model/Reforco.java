package model;


import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Entity
@Table(name="reforco")
public class Reforco {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private Date data;


    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "rf_produto",
            joinColumns = { @JoinColumn(name = "reforco_id") },
            inverseJoinColumns = { @JoinColumn(name = "produto_id") }
    )
    Set<Produto> produtos = new HashSet<>();


    @OneToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="tcp_id", unique= true, nullable=true, insertable=true, updatable=true)
    private Tcp tcp;

    private Enum Vendedor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Set<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(Set<Produto> produtos) {
        this.produtos = produtos;
    }

    public Tcp getTcp() {
        return tcp;
    }

    public void setTcp(Tcp tcp) {
        this.tcp = tcp;
    }

    public Enum getVendedor() {
        return Vendedor;
    }

    public void setVendedor(Enum vendedor) {
        Vendedor = vendedor;
    }
}
