package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;

public class initialC {

    @FXML public PasswordField pwfield;
    @FXML public TextField lgfield;
    @FXML public Button btLg;
    @FXML public MenuBar menubLogin;
    @FXML public Label lgL;
    @FXML public Label pwL;
    @FXML public MenuItem mnLogout;
    @FXML public MenuItem tcpC;

    @FXML public void Login(ActionEvent event){
        System.out.println("passou 1");
        if(lgfield.getText().contains("admin") && pwfield.getText().contains("admin")){
            System.out.println("Passou");
            menubLogin.setVisible(true);
            FormVisible(false);
            lgfield.setText("");
            pwfield.setText("");
        }else {
            JOptionPane.showMessageDialog(null, "Login Ou Senha Incorreta,tente novamente!" );
            menubLogin.setVisible(false);
        }
    }

    @FXML public void Logout(ActionEvent event){
        System.out.println("entrou logout");
        FormVisible(true);
        menubLogin.setVisible(false);
    }
    @FXML public void FormVisible(Boolean b){
        pwfield.setVisible(b);
        lgfield.setVisible(b);
        btLg.setVisible(b);
        lgL.setVisible(b);
        pwL.setVisible(b);
    }

    @FXML public void goToTcpC(ActionEvent event){
        try {

            Parent root = FXMLLoader.load(getClass().getResource("../view/tcp.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root, 1000,600);
            stage.setScene(scene);
            stage.show();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
