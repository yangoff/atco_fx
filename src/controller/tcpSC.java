package controller;

import dao.TcpDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import model.Tcp;

public class tcpSC {

    @FXML
    public TextField nomeTcpF;

    @FXML
    public TextField tcpRegF;

    @FXML
    public ChoiceBox tcpLvlF;

    @FXML
    public Button tcpSaveBt;

    @FXML
    public Button tcpClearBt;

    ObservableList<String> nivel = FXCollections.observableArrayList("1","2","3","4");

    @FXML
    public void initialize(){
        tcpLvlF.setValue("1");
        tcpLvlF.setItems(nivel);
    }

    @FXML public void saveTcp(){
        Tcp tcp = new Tcp(
                new String(nomeTcpF.getText()),
                Integer.parseInt((String) tcpLvlF.getValue()),
                Integer.parseInt(tcpRegF.getText())
                );


        System.out.println(tcp.toString());
        TcpDAO tcpD = new TcpDAO();
        tcpD.inserirTcp(tcp);

    }

    @FXML public void clear(){
        nomeTcpF.setText("");
        tcpRegF.setText("");
        tcpLvlF.setValue("1");
    }
}
