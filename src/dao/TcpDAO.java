package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import model.Tcp;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TcpDAO {

    @PersistenceContext
    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("atco");
    EntityManager entityManager = entityManagerFactory.createEntityManager();

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    public Tcp inserirTcp(Tcp tcp) {
        try {
           entityManager.persist(tcp);
           entityManager.getTransaction().commit();

        }catch (Exception e){
            System.out.println(e.getMessage()+ "/"+e.getCause());
        }
        return tcp;
    }

    public Tcp updateTcp(Tcp tcp){
        entityManager.merge(tcp);
        return tcp;
    }

    public Tcp deleteTcp(Tcp tcp){
        entityManager.remove(tcp);
        return tcp;
    }

}
